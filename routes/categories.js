const express = require('express');
const router = express.Router();
const fs = require('fs');

/* GET categories */
router.get('/', function (req, res) {
    fs.readFile(__dirname + '/../data/data.json', 'utf8', (err, data) => {
        res.end(data);
    })
});

module.exports = router;
